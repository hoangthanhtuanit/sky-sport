/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js}"],
  darkMode: "class", // or 'media' or 'class'
  theme: {
    backgroundImage: {
      'pricing': "url('/assets/img/bg-pricing.png')",
    },
    container: {
      center: true,
      // padding: "16px",
    },
    extend: {
      colors: {
        primary: "#14b8a6",
        secondary: "#64748b",
        neutral: "#f3f4f6",
        vercel: "#0284c7",
        netlify: "#2563eb",
        github: "#57534e",
        coffee: "#d97706",
        kofi: "#db2777",
        dark: "#0f172a",
        darkslategray: "#3a3838",
        "light-colors-neutral-gray-content-1": "#1c1c1c",
        white: "#FFFFFF",
        gainsboro: "#e6e6e6",
        "dark-gray": "#636363",
        lightgray: {
          "100": "#d1d3d4",
          "200": "rgba(209, 211, 212, 0.5)",
        },
        "slight-magenta": "#B463A6",
        "dark-colors-neutral-gray-background-1": "#272727",
        icon: "#a4a4a4",
        darkgray: "#a6a6a6",
        goldenrod: "#f5c451",
        black: "#100f0f",
        base: "#B463A6",
        neutralGray: '#272727',
      },
      screens: {
        'xl': "1280px",
        '2xl': "1492px"
      },
      fontFamily: {
        inter: "Inter",
        roboto: "Roboto",
        poppins: "Poppins",
      },
      borderRadius: {
        "3xs": "10px",
      },
      fontSize: {
        sm: "14px",
        xs: "12px",
        base: "16px",
        "5xl": "24px",
        smi: "13px",
        xl: "20px",
        xli: "18px",
        inherit: "inherit",
      },
    },
  },
  plugins: [
    'autoprefixer',
    'prettier-plugin-tailwindcss',
    function ({ addComponents }) {
      addComponents({
        '.container': {
          maxWidth: '100%',
          '@screen sm': {
            maxWidth: '100%',
          },
          '@screen md': {
            maxWidth: '100%',
          },
          '@screen lg': {
            maxWidth: '100%',
          },
          '@screen xl': {
            maxWidth: '100%',
          },
          '@screen 2xl': {
            maxWidth: '1492px',
          },
        }
      })
    }
  ],
};
