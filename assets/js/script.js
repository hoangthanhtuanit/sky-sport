// // Navbar Fixed
// window.onscroll = function () {
//   const header = document.querySelector("header");
//   const bottomNav = document.querySelector("#bottom-navigation");
//   const fixedNav = header.offsetTop;
//   const toTop = document.querySelector("#to-top");
//
//   if (window.pageYOffset > fixedNav) {
//     header.classList.add("navbar-fixed");
//     bottomNav.classList.add("navbar-bottom");
//     toTop.classList.remove("hidden");
//     toTop.classList.add("flex");
//   } else {
//     header.classList.remove("navbar-fixed");
//     bottomNav.classList.remove("navbar-bottom");
//     toTop.classList.remove("flex");
//     toTop.classList.add("hidden");
//   }
// };
//
// // Dark Mode
// const darkToggle = document.querySelector("#dark-toggle");
// const html = document.querySelector("html");
//
// if (darkToggle) {
//   darkToggle.addEventListener("click", function () {
//     if (darkToggle.checked) {
//       html.classList.add("dark");
//       localStorage.theme = "dark";
//     } else {
//       html.classList.remove("dark");
//       localStorage.theme = "light";
//     }
//   });
//
//   // TOGGLE BUTTON DARK MODE (LOCAL STORAGE) BASED ON USER PREFERENCE
//   // On page load or when changing themes, best to add inline in `head` to avoid FOUC
//   darkToggle.checked = localStorage.theme === "dark" || (!("theme" in localStorage) && window.matchMedia("(prefers-color-scheme: dark)").matches);
// }

$(document).ready(function ($) {
    $('.owl-carousel.follow-club').owlCarousel({
		autoWidth: false,
		autoplay: true,
		autoplayTimeout: 3000,
		autoplayHoverPause: true,
		nav: true,
		navText: false,
		items: 4,
		responsive: {
			0: {
				dots: true,
				nav: true,
				items: 3.1
			},
			430: {
				dots: true,
				nav: true,
				items: 4.6
			},
			1024: {
				items: 7
			}
		},
		lazyLoad: true
	});
    $('.owl-carousel.on-air-matches').owlCarousel({
		autoWidth: false,
		autoplay: true,
		autoplayTimeout: 3000,
		autoplayHoverPause: true,
		nav: true,
		navText: false,
		items: 4,
		responsive: {
			0: {
				dots: true,
				nav: true,
				items: 1.5
			},
			430: {
				dots: true,
				nav: true,
				items: 1.5
			},
			1024: {
				items: 3
			}
		},
		lazyLoad: true
	});
});

const accordionHeader = document.querySelectorAll('.accordion-header')

accordionHeader.forEach(accordionHeader => {
    accordionHeader.addEventListener("click", event => {
        accordionHeader.classList.toggle("active")
        const accordionBody = accordionHeader.nextElementSibling
        if (accordionHeader.classList.contains("active")) {
            accordionBody.style.maxHeight = accordionBody.scrollHeight + "px"
        } else {
            accordionBody.style.maxHeight = 0
        }
    })
})

var hidePassword= document.querySelector(".eye_hide");
var password= document.querySelector(".password");
var setShowPassword= document.querySelector(".eye_hide");

if (hidePassword) {
    hidePassword.addEventListener('click',function(){
        if (password.type === "password") {
            password.type = "text";
            setShowPassword.classList.remove('bxs-hide');
            setShowPassword.classList.add('bxs-show');
        } else {
            password.type = "password";
            setShowPassword.classList.add('bxs-hide');
            setShowPassword.classList.remove('bxs-show');
        }
    });
}

var hideRePassword= document.querySelector(".eye-re-password-hide");
var rePassword= document.querySelector(".re-password");
var setShowRePassword= document.querySelector(".eye-re-password-hide");

if (hideRePassword) {
    hideRePassword.addEventListener('click',function(){
        if (rePassword.type === "password") {
            rePassword.type = "text";
            setShowRePassword.classList.remove('bxs-hide');
            setShowRePassword.classList.add('bxs-show');
        } else {
            rePassword.type = "password";
            setShowRePassword.classList.add('bxs-hide');
            setShowRePassword.classList.remove('bxs-show');
        }
    })
}

$('.sidebar-mobile__action').click(function (e) {
    if ($('.sidebar-mobile').hasClass("active")) {
        $('.sidebar-mobile').removeClass("active");
    }
    else {
        $('.sidebar-mobile').addClass("active");
    }
});
